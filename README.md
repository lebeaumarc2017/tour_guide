**Prérequis:**

Java 8
Gradle
JDK 17

Lancer application

Telecharger ou cloner le projet

**Test**

Une fois dans le dossier du projet executer la commande gradlew build dans le terminal.

ou

Utiliser gitlab.com : CI/CD pipelines


**Url**

Dans un naviguateur taper:

 http://localhost:8080/getAllCurrentLocations   Liste de tout les utilisateurs et leurs positions.

 http://localhost:8080/getNearbyAttractions/?userName=internalUser0  Liste des 5 attractions les plus proche de l'utilisateurs

 http://localhost:8080/getLocation?userName=internalUser0 Récuperation de la localisation de l'utilisateur ayant comme userName internalUser0 

 http://localhost:8080/getRewards?userName=internalUser0 Récupere les points de récompense de l'utilisateur ayant comme userName internalUser0 

 http://localhost:8080/getTripDeals?userName=internalUser0 Liste des propositions d'attraction suivant les préférences de l'utilisateur ayant comme userName internalUser0.

 http://localhost:8080/getUser?userName=internalUser0 Récupere les informations de l'utilisateur ayant comme userName internalUser0.
