package tourGuide;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tourGuide.user.UserReward;

public class TestPerformance {
	
	
	@Before
	public void setUp() {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
		Locale.setDefault(new Locale("en", "US"));
	}
	
	/*
	 * A note on performance improvements:
	 *     
	 *     The number of users generated for the high volume tests can be easily adjusted via this method:
	 *     
	 *     		InternalTestHelper.setInternalUserNumber(100000);
	 *     
	 *     
	 *     These tests can be modified to suit new solutions, just as long as the performance metrics
	 *     at the end of the tests remains consistent. 
	 * 
	 *     These are performance metrics that we are trying to hit:
	 *     
	 *     highVolumeTrackLocation: 100,000 users within 15 minutes:
	 *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
	 *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 */
	
	/*
	
	@Ignore
	@Test
	public void highVolumeTrackLocationTest() throws InterruptedException {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		InternalTestHelper.setInternalUserNumber(100);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

		List<User> allUsers = new ArrayList<>();
		allUsers = tourGuideService.getAllUsers();
		
	    StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		// améliorer
		
		
		List<Thread> listThread = new ArrayList<Thread>();
		for (int i = 0; i<allUsers.size(); i++) {
			TourGuideService.TrackUserLocationThread trackUserLocationThread = tourGuideService.new TrackUserLocationThread(allUsers.get(i));
			 listThread.add(trackUserLocationThread);
		}
		
		for (int i = 0; i<allUsers.size(); i++) {
			listThread.get(i).start();
		}
		
		for (int i = 0; i<allUsers.size(); i++) {
			listThread.get(i).join();
		}
		
		/*
		for(User user : allUsers) {
			tourGuideService.trackUserLocation(user);
		}
		
		
		
		// améliorer
		stopWatch.stop();
		
		tourGuideService.tracker.stopTracking();
		
		

		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		//assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
		assertTrue(TimeUnit.MILLISECONDS.toSeconds(900) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}
	
	
	
	@Ignore
	@Test
	public void highVolumeGetRewardsTest() throws InterruptedException {
		
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());

		// Users should be incremented up to 100,000, and test finishes within 20 minutes
		InternalTestHelper.setInternalUserNumber(1000);
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		
		List<Thread> listThread = new ArrayList<Thread>();
		
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
		
	    Attraction attraction = gpsUtil.getAttractions().get(0);
		List<User> allUsers = new ArrayList<>();
		allUsers = tourGuideService.getAllUsers();
		allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));
		
		for (int i = 0; i<allUsers.size(); i++) {
			RewardsService.CalculateRewardsThread calculateRewardsThread = rewardsService.new CalculateRewardsThread(allUsers.get(i));
			 listThread.add(calculateRewardsThread);
		}
		
		for (int i = 0; i<allUsers.size(); i++) {
			listThread.get(i).start();
		}
		
		for (int i = 0; i<allUsers.size(); i++) {
			
				listThread.get(i).join();
			
			
		}
		
		for (int i = 0; i<allUsers.size(); i++) {
			//System.err.println("############# "+ i + " : "+ allUsers.get(i).getUserRewards().size() );
			assertTrue(allUsers.get(i).getUserRewards().size() > 0);
			//System.err.println("############# "+ i + " : "+ allUsers.get(i).getUserRewards().size() );
		}
		
		stopWatch.stop();
		tourGuideService.tracker.stopTracking();
		
		System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		//assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
		assertTrue(TimeUnit.MILLISECONDS.toSeconds(6000) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
		

		
		
	     
	   // allUsers.forEach(u -> rewardsService.calculateRewards(u));
	    
		
	    
		
		
		
		

		
		
	}
	
*/

	//@Ignore
	@Test
	public void highVolumeTrackLocation() throws InterruptedException {
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());
		// Users should be incremented up to 100,000, and test finishes within 15 minutes
		InternalTestHelper.setInternalUserNumber(1000);
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

		List<User> allUsers = new ArrayList<>();
		allUsers = tourGuideService.getAllUsers();
		
	    StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		// améliorer
		

		
		
		for(User user : allUsers) {
			tourGuideService.trackUserLocation(user);
		}
		
		
		
		// améliorer
		stopWatch.stop();
		
		tourGuideService.tracker.stopTracking();
		
		

		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		//assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
		assertTrue(TimeUnit.MILLISECONDS.toSeconds(9000) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}
	
	//@Ignore
	@Test
	public void highVolumeGetRewards() throws InterruptedException {
		
		GpsUtil gpsUtil = new GpsUtil();
		RewardsService rewardsService = new RewardsService(gpsUtil, new RewardCentral());

		// Users should be incremented up to 100,000, and test finishes within 20 minutes
		InternalTestHelper.setInternalUserNumber(1000);
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		
		
		TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);
		
	    Attraction attraction = gpsUtil.getAttractions().get(0);
		List<User> allUsers = new ArrayList<>();
		allUsers = tourGuideService.getAllUsers();
		
		allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));
		
		allUsers.forEach(u -> rewardsService.calculateRewards(u));
		
		
		
		
		for (int i = 0; i<allUsers.size(); i++) {
			//System.err.println("############# "+ i + " : "+ allUsers.get(i).getUserRewards().size() );
			assertTrue(allUsers.get(i).getUserRewards().size() > 0);
			//System.err.println("############# "+ i + " : "+ allUsers.get(i).getUserRewards().size() );
		}
		
		
		
		
		stopWatch.stop();
		tourGuideService.tracker.stopTracking();
		
		System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		//assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
		assertTrue(TimeUnit.MILLISECONDS.toSeconds(12000) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
		

		
		
	     
	    
	    
		
	    
		
		
		
		

		
		
	}
	
}
