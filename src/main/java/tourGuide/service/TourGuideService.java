package tourGuide.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.money.Monetary;

import org.apache.commons.lang3.time.StopWatch;
import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.helper.InternalTestHelper;
//import tourGuide.service.TourGuideService.TrackUserLocationThread;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

@Service
public class TourGuideService {
	private Logger logger = LoggerFactory.getLogger(TourGuideService.class);
	private final GpsUtil gpsUtil;
	private final RewardsService rewardsService;
	private final TripPricer tripPricer = new TripPricer();
	public final Tracker tracker;
	boolean testMode = true;
	public ExecutorService executor = Executors.newCachedThreadPool();
	
	
	
	public TourGuideService(GpsUtil gpsUtil, RewardsService rewardsService) {
		this.gpsUtil = gpsUtil;
		this.rewardsService = rewardsService;
		
		if(testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}
	
	public List<UserReward> getUserRewards(User user) {
		return user.getUserRewards();
	}
	
	public VisitedLocation getUserLocation(User user) {
		/*
		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ?
			user.getLastVisitedLocation() : trackUserLocation(user);
		*/
		if  (user.getVisitedLocations().size() <= 0) {
			 trackUserLocation(user);
		}
		VisitedLocation visitedLocation = user.getLastVisitedLocation();
		
		return visitedLocation;
	}
	
	public User getUser(String userName) {
		return internalUserMap.get(userName);
	}
	
	public List<User> getAllUsers() {
		return internalUserMap.values().stream().collect(Collectors.toList());
	}
	
	public void addUser(User user) {
		if(!internalUserMap.containsKey(user.getUserName())) {
			internalUserMap.put(user.getUserName(), user);
		}
	}
	
	public List<Provider> getTripDeals(User user) {
		// ICI
		
		int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
		
		List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(), 
				user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
		
		
		user.setTripDeals(providers);
		return providers;
	}
	
	

	
	
	public CompletableFuture<Void> trackUserLocation(User user) {
		
		
		
		CompletableFuture<Void> cf = CompletableFuture.supplyAsync(() -> {
			
		  VisitedLocation visitedLocation =  gpsUtil.getUserLocation(user.getUserId());
		   user.addToVisitedLocations(visitedLocation);
		   //System.out.println("00000"+ user.getVisitedLocations());
		   CompletableFuture<Void> cfreward = rewardsService.calculateRewards(user);
		   
		   try {
				cfreward.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		   
		   
		   
		   return visitedLocation;
		}, executor)
			.thenAccept(visitedLocation -> { 
				//return visitedLocation;
				
				return;
			});
		
		
		
		return cf;
		
		
	}

	public List<Attraction> getNearByAttractions(VisitedLocation visitedLocation) {
		List<Attraction> nearbyAttractions = new ArrayList<>();
		
		// recuperation des 5 plus proches
		
		List<Attraction> nearbyAttractionsGpsUtils = gpsUtil.getAttractions();
		
		// creation d'une liste de distance
		
		List<Double> distances =  new ArrayList<Double>();
		
		for(Attraction attraction : nearbyAttractionsGpsUtils) {
			 distances.add(rewardsService.getDistance(attraction, visitedLocation.location));
		}
		
		// parcourir les distcance pour recuperer les 5 indeices des plus petites distances
		
		List<Integer> listeIndicesMin =  new ArrayList<>(); // but => taille 5
		 
		for (int j = 0; j < 5; j++) {
			
			
			double min = distances.get(0);
			
			int indice = 0;
			
			for (int i = 0; i < distances.size(); i++) {
				if(distances.get(i) < min) {
					min = distances.get(i);
					indice = i;
				}
				
			}
			
			listeIndicesMin.add(indice);
			distances.remove(indice);
		}
		
		for (int i = 0; i < listeIndicesMin.size(); i++) {
			if(rewardsService.isWithinAttractionProximity(nearbyAttractionsGpsUtils.get(i), visitedLocation.location)) {	
							
							nearbyAttractions.add(nearbyAttractionsGpsUtils.get(i));
				}
		}
		
		/*
		for(Attraction attraction : gpsUtil.getAttractions()) {
			if(rewardsService.isWithinAttractionProximity(attraction, visitedLocation.location)) {	
				
				nearbyAttractions.add(attraction);
			}
		}
		*/
		
		
		return nearbyAttractions;
	}
	
	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() { 
		      public void run() {
		        tracker.stopTracking();
		      } 
		    }); 
	}
	
	/**********************************************************************************
	 * 
	 * Methods Below: For Internal Testing
	 * 
	 **********************************************************************************/
	private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
	private final Map<String, User> internalUserMap = new HashMap<>();
	
	private void initializeInternalUsers() {
		// Creation des users
		
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);
			
			// Test ajout user preference adult = 0
			
			/*
			UserPreferences userpref = new UserPreferences();
			
			
			
			userpref.setNumberOfAdults(1);
			userpref.setNumberOfChildren(1);
			userpref.setTripDuration(100);
			
			
			
			user.setUserPreferences(userpref);
			
			*/
			
			internalUserMap.put(userName, user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}
	
	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i-> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}
	
	private double generateRandomLongitude() {
		double leftLimit = -180;
	    double rightLimit = 180;
	    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}
	
	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
	    double rightLimit = 85.05112878;
	    return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}
	
	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
	    return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}
	
}
