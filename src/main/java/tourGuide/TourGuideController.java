package tourGuide;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tripPricer.Provider;

@RestController
public class TourGuideController {

	@Autowired
	TourGuideService tourGuideService;
	
	@Autowired
	RewardsService rewardsService;
	
	
    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }
    
    @RequestMapping("/getLocation") 
    public String getLocation(@RequestParam String userName) {
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
		return JsonStream.serialize(visitedLocation.location);
    }
    
    //  TODO: Change this method to no longer return a List of Attractions.
 	//  Instead: Get the closest five tourist attractions to the user - no matter how far away they are.
 	//  Return a new JSON object that contains:
    	// Name of Tourist attraction, 
        // Tourist attractions lat/long, 
        // The user's location lat/long, 
        // The distance in miles between the user's location and each of the attractions.
        // The reward points for visiting each Attraction.
        //    Note: Attraction reward points can be gathered from RewardsCentral
    
    // String 
    
    @RequestMapping("/getNearbyAttractions") 
    public ResponseEntity<Map<String, Object>> getNearbyAttractions(@RequestParam String userName) {
    	
    	
    	
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
    	//return JsonStream.serialize(tourGuideService.getNearByAttractions(visitedLocation));
    	
    	Map<String, Object> attractions = new HashMap<>();
    	for (Attraction attraction :  tourGuideService.getNearByAttractions(visitedLocation)) {
    		Map<String, Object> infoAttraction = new HashMap<>();
			Double distance = rewardsService.getDistance(attraction, visitedLocation.location);
			
			infoAttraction.put("attraction", attraction);
			infoAttraction.put("distanceUser", distance*1.56+ " km");
			infoAttraction.put("rewards", rewardsService.getRewardPoint(attraction, getUser(userName)));
			
			
    		attractions.put(attraction.attractionName, infoAttraction);
		}
    	
    	
    	Map<String, Object> result = new HashMap<>();
    	
    	result.put("userlocation", getUser(userName).getLastVisitedLocation());
    	result.put("attractions", attractions);
    	
    	return ResponseEntity.ok(result);
    }
    
    @RequestMapping("/getRewards") 
    public String getRewards(@RequestParam String userName) {
    	return JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName)));
    }
    
    @RequestMapping("/preference") 
    public void setPreference(@RequestParam String userName, @RequestParam int adultNumber, @RequestParam int childrenNumber, 
    		@RequestParam int tripDuration) {
    	
    	UserPreferences userpref = new UserPreferences();
		
		
		
		userpref.setNumberOfAdults(adultNumber);
		userpref.setNumberOfChildren(childrenNumber);
		userpref.setTripDuration(tripDuration);
		
		User user = getUser(userName);
		
		user.setUserPreferences(userpref);
    }
    
    
    @RequestMapping("/getAllCurrentLocations")
    public ResponseEntity<Map<String,Location>> getAllCurrentLocations() {
    	// TODO: Get a list of every user's most recent location as JSON
    	//- Note: does not use gpsUtil to query for their current location, 
    	//        but rather gathers the user's current location from their stored location history.
    	//
    	// Return object should be the just a JSON mapping of userId to Locations similar to:
    	//     {
    	//        "019b04a9-067a-4c76-8817-ee75088c3822": {"longitude":-48.188821,"latitude":74.84371} 
    	//        ...
    	//     }
    	
    	Map<String, Location> result = new HashMap<String, Location>();
    	
    	 List<User> users = tourGuideService.getAllUsers();
    	 
    	 for (Iterator iterator = users.iterator(); iterator.hasNext();) {
			User user = (User) iterator.next();
			
			result.put(user.getUserId().toString(), user.getLastVisitedLocation().location);
			
		}
    	return ResponseEntity.ok(result);
    	//return JsonStream.serialize(result);
    }
    
    @RequestMapping("/getTripDeals")
    public ResponseEntity<List<Provider>> getTripDeals(@RequestParam String userName) {
    	List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
    	//return JsonStream.serialize(providers);
    	return ResponseEntity.ok(providers);
    	//return JsonStream.serialize("");
    }
    
    private User getUser(String userName) {
    	return tourGuideService.getUser(userName);
    }
   

}